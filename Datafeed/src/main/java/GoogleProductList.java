import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.*;
import model.MyProduct;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.*;

/**
 * Created by spineor on 21/11/17.
 */
public class GoogleProductList {

    private static final BigInteger merchantId = new BigInteger("115161870");
    static Map map =new HashMap<String,String>();
    public static void main(String[] args) throws JAXBException, IOException, GeneralSecurityException {
        Authorization authorization = new Authorization();
        ShoppingContent service = authorization.getShoppingContent();
        getCondition();
        getProductList(merchantId,service);
    }

    public static void getProductList(BigInteger merchantId, ShoppingContent content)throws  IOException{
        ShoppingContent.Products.List productsList = content.products().list(merchantId).setMaxResults(250L);
        int batch =0;
        int i=0,j=0;
        do {
            ProductsListResponse page = productsList.execute();
            if (page.getResources() == null) {
                System.out.println("No products found.");
                return;
            }
            i++;
            List<Product> products = new ArrayList<Product>();
            for (Product product : page.getResources()) {
                if(product.getMpn() == null) {
                    //System.out.println(product);
                    continue;
                    //content.products().delete(new BigInteger("115161870"), product.getId()).execute();
                }
                products.add(insertfeed(content,product));
            j++;
            }

            ProductsCustomBatchResponse batchResponse =
                    content.products().custombatch(createBatch(products)).execute();

            batch++;
            System.out.println("processing batch "+batch);
            if (page.getNextPageToken() == null) {
                System.out.println("finished");
                break;
            }
            productsList.setPageToken(page.getNextPageToken());

        } while (true);
    }


    public static Product insertfeed(ShoppingContent content, Product product) throws  IOException{
        Product newProduct = new Product();
        System.out.println(product.getId());
        newProduct.setContentLanguage(product.getContentLanguage());
        newProduct.setTargetCountry(product.getTargetCountry());
        newProduct.setChannel(product.getChannel());
        newProduct.setOfferId(product.getOfferId());
        newProduct.setId(product.getId());
        newProduct.setCustomLabel0(product.getCustomLabel0());
        newProduct.setCustomLabel1(product.getCustomLabel1());
        newProduct.setMpn(product.getMpn());
        newProduct.setGoogleProductCategory(product.getGoogleProductCategory());
        newProduct.setGtin(product.getGtin());
        newProduct.setProductType(product.getProductType());
        newProduct.setImageLink(product.getImageLink());
        newProduct.setAvailability(product.getAvailability());
        newProduct.setPrice(product.getPrice());
        newProduct.setItemGroupId(product.getItemGroupId());
        newProduct.setDescription(product.getDescription());
        newProduct.setBrand(product.getBrand() != null ? (product.getBrand().equals("Generic") ? "Replace" : product.getBrand()): "");
        newProduct.setTitle(product.getTitle().contains("Generic") ? product.getTitle().replace("(Generic) ","").toString() : product.getTitle().toString());
        newProduct.setShipping(product.getShipping());
        String link = product.getLink().contains("Generic") ? product.getLink().replace("Generic","Replace").toString() : product.getLink().toString();
        String adwords = "https://tracking.theautopartsshop.com/?campaignid={campaignid}&adgroupid={adgroupid}&feeditemid={feeditemid}&targetid={targetid}&matchtype={matchtype}&network={network}&device={device}&devicemodel={devicemodel}&creative={creative}&keyword={keyword}&placement={placement}&target={target}&adposition={adposition}&url=" +link;
        newProduct.setLink(link);
        newProduct.setAdwordsRedirect(adwords);
        newProduct.setCondition(map.get(product.getId()).toString());
        //System.out.println(newProduct);
        return newProduct;
        //Product result = content.products().insert(new BigInteger("115161870"), product).execute();
        //System.out.printf("Product %s inserted\n", result.getOfferId());
    }

    public static Map<String,String> getCondition()throws IOException{
        String splitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader("/home/spineor/Downloads/part-00000-98c5c9f8-d9d9-41d8-a5ab-762a7a250b9c.csv"));
        String line = "";

        while ((line = br.readLine()) !=null) {
            String[] str = line.split(",");
            if(str.length == 1) {
                map.put(str[0], "new");
            }
            else {
                map.put(str[0], str[1]);
            }
        }
        return map;
    }
    public static ProductsCustomBatchRequest createBatch(List<Product> products) {
        List<ProductsCustomBatchRequestEntry> productsBatchRequestEntries =
                new ArrayList<ProductsCustomBatchRequestEntry>();
        for (int i=0;i<products.size();i++) {
            productsBatchRequestEntries.add(
                    new ProductsCustomBatchRequestEntry()
                            .setBatchId((long) i)
                            .setMerchantId(merchantId)
                            .setProduct(products.get(i))
                            .setMethod("insert"));
        }
        ProductsCustomBatchRequest batchRequest = new ProductsCustomBatchRequest();
        batchRequest.setEntries(productsBatchRequestEntries);
        return batchRequest;
    }

}
