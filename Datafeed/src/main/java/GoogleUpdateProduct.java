import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.*;
import model.Channel;
import model.Item;
import model.OurPrice;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.naming.spi.ObjectFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by spineor on 27/9/17.
 */
public class GoogleUpdateProduct {

    private static final BigInteger merchantId = new BigInteger("115161870");
    private static final int PRODUCT_COUNT = 10;

    public static void main(String[] args)throws JAXBException, IOException, GeneralSecurityException {

        Authorization authorization = new Authorization();
        ShoppingContent service = authorization.getShoppingContent();
        insertProducts(service);
        //authorization.productWrapper(sc,);
        //.listProduct(sc);

    }
    //"https://images.theautopartsshop.com/wp/hires/W01331610508OES.JPG"
    public static void insertProducts(ShoppingContent service)throws JAXBException, IOException{
                    Product product = new Product();
                    //product.setId("103967325");
                    product.setOfferId("3ddf5bcf-465c-4a60-9d44-0e4c496ae94d");
                    product.setTitle("1999 Jaguar XJ8 Wheel");
                    product.setDescription("Alloy wheel; 16 x 7; 20 spokes; 5 lug; 4.75 inch bp; aftermarket chrome. It fits sub-model(s) (L, 2+2, Base) and (6C 4.0L , 8C 4.0L , 12C 6.0L ) engine(s).");
                    product.setLink("https://theautopartsshop.com/sku/Generic/Wheel/ALY59725U85/1999/Jaguar/XJ8");
                    product.setImageLink("https://images.theautopartsshop.com/lkq/886/ALY59725U85.jpg");
                    product.setContentLanguage("en");
                    product.setTargetCountry("US");
                    product.setChannel("online");
                    product.setProductType("Tire and Wheel > Wheel > Wheel");
                    product.setAvailability("in stock");
                    product.setCondition("new");
                    //product.setGoogleProductCategory(item.getProduct_type());
                    product.setGtin("840304078929");
                    product.setGoogleProductCategory("2556");
                    product.setAdwordsRedirect("https://tracking.theautopartsshop.com/?campaignid={campaignid}&adgroupid={adgroupid}&feeditemid={feeditemid}&targetid={targetid}&matchtype={matchtype}&network={network}&device={device}&devicemodel={devicemodel}&creative={creative}&keyword={keyword}&placement={placement}&target={target}&adposition={adposition}&url=https://theautopartsshop.com/sku/Generic/Wheel/ALY59725U85/1999/Jaguar/XJ8");
                    product.setCustomLabel0("");
                    product.setCustomLabel1("");


                    Price price = new Price();
                    price.setValue("386.60");
                    price.setCurrency("USD");
                    product.setPrice(price);


                    Price shippingPrice = new Price();
                    shippingPrice.setValue("0.0");
                    shippingPrice.setCurrency("USD");

                    ProductShipping shipping = new ProductShipping();
                    shipping.setPrice(shippingPrice);
                    shipping.setCountry("US");
                    shipping.setService("Standard shipping");

                    ArrayList shippingList = new ArrayList();
                    shippingList.add(shipping);
                    product.setShipping(shippingList);

                    ProductsCustomBatchResponse batchResponse =
                            service.products().custombatch(createBatch()).execute();

                    System.out.println("Insert Response: " + batchResponse);
                }
    public static ProductsCustomBatchRequest createBatch() {
        List<ProductsCustomBatchRequestEntry> productsBatchRequestEntries =
                new ArrayList<ProductsCustomBatchRequestEntry>();
        for (int i = 0; i < PRODUCT_COUNT; i++) {
            /*Product product = ExampleProductFactory.create(config, prefix + i);
            productsBatchRequestEntries.add(
                    new ProductsCustomBatchRequestEntry()
                            .setBatchId((long) i)
                            .setMerchantId(merchantId)
                            .setProduct(product)
                            .setMethod("insert"));*/
        }
        ProductsCustomBatchRequest batchRequest = new ProductsCustomBatchRequest();
        batchRequest.setEntries(productsBatchRequestEntries);
        return batchRequest;
    }

}


