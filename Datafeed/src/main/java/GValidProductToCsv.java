import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.*;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.*;

/**
 * Created by spineor on 21/11/17.
 */
public class GValidProductToCsv {

    private static final BigInteger merchantId = new BigInteger("115161870");
    static Map map =new HashMap<String,String>();
    public static void main(String[] args) throws JAXBException, IOException, GeneralSecurityException {
        Authorization authorization = new Authorization();
        ShoppingContent service = authorization.getShoppingContent();
        getProductList(merchantId,service);
    }

    public static void getProductList(BigInteger merchantId, ShoppingContent content)throws  IOException{
        ShoppingContent.Products.List productsList = content.products().list(merchantId).setMaxResults(250L);
        Set set = new HashSet<String>();
        int i=0,j=0;
        do {
            ProductsListResponse page = productsList.execute();
            if (page.getResources() == null) {
                System.out.println("No products found.");
                return;
            }

            i++;
            for (Product product : page.getResources()) {
                set.add(getProduct(product));
                j++;
            }

            System.out.println("PageNo :"+i+"Product No:" +j);
            if (page.getNextPageToken() == null) {
                System.out.println("finished");
                break;
            }
            productsList.setPageToken(page.getNextPageToken());

        } while (true);
        //CsvFileWriter.writeCsvFile(set);
    }
    public static String getProduct(Product product) {

        return product.getId() + "|" +
                product.getOfferId() + "|" +
                product.getTitle() + "|" +
                product.getLink() + "|" +
                product.getImageLink() + "|" +
                product.getContentLanguage() + "|" +
                product.getTargetCountry() + "|" +
                product.getChannel() + "|" +
                product.getAvailability() + "|" +
                product.getBrand() + "|" +
                product.getCondition() + "|" +
                product.getGoogleProductCategory() + "|" +
                product.getGtin() + "|" +
                product.getItemGroupId() + "|" +
                product.getMpn() + "|" +
                product.getPrice().getCurrency() + "|" +
                product.getPrice().getValue() + "|" +
                product.getProductType() + "|" +
                product.getShipping().get(0).getPrice().getCurrency() + "|" +
                product.getShipping().get(0).getPrice().getValue() + "|" +
                product.getShipping().get(0).getCountry() + "|" +
                product.getAdwordsRedirect() + "|" +
                product.getCustomLabel0() + "|" +
                product.getCustomLabel1() + "|" +
                product.getDescription() + "|" +
                product.getProductType();

    }


}
