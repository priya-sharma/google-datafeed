import com.google.api.services.content.model.Price;
import com.google.api.services.content.model.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by spineor on 4/10/17.
 */
public class CsvFileReader {
    public static void main(String[] args)throws IOException{
        String splitBy = "^";
        System.out.println("Reading File");
        BufferedReader br = new BufferedReader(new FileReader("/home/spineor/Downloads/cart/Datafeed/src/main/resources/test.csv"));
        System.out.println("Read Lines");
        String line = "";
        System.out.println("line: " +line);
        while ((line = br.readLine()) !=null) {
            String[] colValue = line.split("\\^");

            System.out.println("OfferId: " + colValue[0]);
            System.out.println("Title: " + colValue[1]);
            System.out.println("Title Length: " + colValue[1].length());
            System.out.println("Description: " + colValue[2]);
            System.out.println("Link: " + colValue[3]);
            System.out.println("ImageLink: " + colValue[4]);
            System.out.println("Availability: " + colValue[5]);
            System.out.println("Price: " + colValue[6]);
            System.out.println("Google Product Category: " + colValue[7]);
            System.out.println("Product Type: " + colValue[8]);
            System.out.println("Brand: " + colValue[9]);
            System.out.println("Gtin: " + colValue[10]);
            System.out.println("Mpn: " + colValue[11]);
            System.out.println("Condition: " + colValue[12]);
            System.out.println("ItemGroupId: " + colValue[13]);
            System.out.println("Custom Label 0: " + colValue[14]);
            System.out.println("Custom Label 1: " + colValue[14]);
        }
    }


}
