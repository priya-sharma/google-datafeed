import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.Price;
import com.google.api.services.content.model.Product;
import model.OurPrice;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.io.IOException;
/**
 * Created by spineor on 27/9/17.
 */
public class CsvToObject {

    private static final BigInteger merchantId = new BigInteger("115161870");

    public static void main(String[] args)throws IOException, GeneralSecurityException {

        Authorization authorization = new Authorization();
        ShoppingContent service = authorization.getShoppingContent();
        insertProducts(service);
        //authorization.productWrapper(sc,);
        //.listProduct(sc);

    }
/*"id", "title", "description", "link", "image_link", "availability", "price currency=\"USD\"",
        "google_product_category", "product_type", "brand", "gtin", "mpn", "condition",
        "item_group_id", "custom_label_0", "custom_label_1"*/
    public static void insertProducts(ShoppingContent service)throws IOException{

        String splitBy = "^";
        BufferedReader br = new BufferedReader(new FileReader("/home/spineor/Downloads/cart/Datafeed/src/main/resources/test.csv"));
        String line = br.readLine();

        while ((line = br.readLine()) !=null) {
            String[] colValue = line.split(splitBy);

            Product product = new Product();

            product.setOfferId(colValue[0]);
            product.setTitle(colValue[1]);
            product.setDescription(colValue[2]);
            product.setLink(colValue[3]);
            product.setImageLink(getImageUrl(colValue[4]));
            product.setAvailability(colValue[5]);
            product.setGoogleProductCategory(colValue[7]);
            product.setProductType(colValue[8]);
            product.setBrand(colValue[9]);
            product.setGtin(colValue[10]);
            product.setMpn(colValue[11]);
            product.setCondition(colValue[12]);
            product.setItemGroupId(colValue[13]);
            product.setCustomLabel0(colValue[14]);
            product.setCustomLabel1(colValue[15]);
                product.setContentLanguage("en");
                product.setTargetCountry("US");
                product.setChannel("online");




                Price price = new Price();
                price.setValue(colValue[6]);
                price.setCurrency("USD");
                product.setPrice(price);

                /*ProductShipping shipping = new ProductShipping();
                shipping.setPrice(shippingPrice);
                shipping.setCountry("US");
                shipping.setService("Standard shipping");

                ArrayList shippingList = new ArrayList();
                shippingList.add(shipping);
                product.setShipping(shippingList);
*/
            Product result = service.products().insert(merchantId, product).execute();
            System.out.println("Insert Response: " + result);

        }
    }

    public static String getImageUrl(String url) throws IOException{
        String newUrl = "https://images.theautopartsshop.com/pa/no-image.png";
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        if(response.getStatusLine().getStatusCode() == 200){
            newUrl = url;
        }
        return newUrl;
    }

    public static OurPrice priceSplit(String price){
        OurPrice oprice = new OurPrice();
        oprice.setPriceValue(price.split(" ")[0]);
        oprice.setPriceCurrency(price.split(" ")[1]);
        return oprice;
    }
}


