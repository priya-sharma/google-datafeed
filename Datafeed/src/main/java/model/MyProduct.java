package model;

/**
 * Created by spineor on 21/11/17.
 */
public class MyProduct {
    private String price;
    private String mpn;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }
}
