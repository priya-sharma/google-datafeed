package model;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

/**
 * Created by spineor on 27/9/17.
 */
public class SitemapXmlRead {

    private  String gapiUrl = "http://devgapi.enetdefender.com/graphql";
    private String queryValuePrefix = " { store {sku(";
    private String getQueryValuesufix = "){PartNumber Title Availability Description imageList Manufacturer_number Category Subcategory DimensionUOM WeightUOM Length Height Width Weight ItemLevelGTIN price brand core_deposit brand_type condition fitmentuid product_url OEPartNumber part_type Part position ImageURL certification}}}";

    public static void main(String[] args)throws JAXBException, IOException, GeneralSecurityException {
        SitemapXmlRead sitemapXmlRead = new SitemapXmlRead();
        sitemapXmlRead.readUrls();
    }

    public void readUrls()throws JAXBException, IOException{

        String[] fileList = new String[]{
                "diamond_standard_1.xml",
                "generic_10.xml",
                "generic_11.xml",
                "generic_12.xml",
                "generic_13.xml",
                "generic_14.xml",
                "generic_15.xml",
                "generic_16.xml",
                "generic_17.xml",
                "generic_18.xml",
                "generic_19.xml",
                "generic_1.xml",
                "generic_20.xml",
                "generic_2.xml",
                "generic_3.xml",
                "generic_4.xml",
                "generic_5.xml",
                "generic_6.xml",
                "generic_7.xml",
                "generic_8.xml",
                "generic_9.xml",
                "goodmark_1.xml",
                "goodmark_2.xml",
                "goodmark_3.xml",
                "goodmark_4.xml",
                "goodmark_5.xml",
                "goodmark_6.xml",
                "oea_1.xml",
                "oes_1.xml",
                "oes_2.xml",
                "oes_3.xml",
                "oes_4.xml",
                "platinum_plus_1.xml",
                "platinum_plus_2.xml",
                "platinum_plus_3.xml",
                "platinum_plus_4.xml",
                "platinum_plus_5.xml",
                "valueline_1.xml",
                "valueline_2.xml",
                "valueline_3.xml"
        };

        JAXBContext context = JAXBContext.newInstance(Urlset.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        //for (String file:fileList) {
            //Sitemapindex e = (Sitemapindex) unmarshaller.unmarshal(new File("/home/spineor/Downloads/sitemap/sitemap/" + file));
            Urlset urlset = (Urlset) unmarshaller.unmarshal(new File("/home/spineor/Downloads/cart/Datafeed/src/main/resources/oea_1.xml"));
            System.out.println("----------File : oea.xml ---------");
            for (Url sitemap : urlset.getUrlList()) {
                int statusCode = checkUrl(sitemap.getLoc());
                System.out.println("****");

                    System.out.println("List of urls no found");
                    System.out.println(sitemap.getLoc());

            }
        System.out.println("----------End of File : oea.xml ---------");
        //}
    }

    public int checkUrl(String url) throws IOException{
        HttpClient client = new DefaultHttpClient();

        HttpPost request = new HttpPost(gapiUrl);
        HttpResponse response = client.execute(request);


        return response.getStatusLine().getStatusCode();
    }

    public static SkuRequest getSkuArgs(String url){
        SkuRequest skuRequest = new SkuRequest();
        String[] arr = url.split("\\/");

        String brand = arr[4];
        String partType = arr[5];
        String partNumber = "";
        String position = "";

        // if array count isgreater that 7 it means position is there
        if(arr.length > 7){
            position = arr[6];
            partNumber = arr[7];
        }
        else{
            partNumber = arr[6];
        }
        skuRequest.setBrand(brand);
        skuRequest.setPartNumber(partNumber);
        skuRequest.setPartType(partType);
        skuRequest.setPosition(position);
        return skuRequest;
    }

}


