package model;

/**
 * Created by spineor on 6/10/17.
 */
public class SkuRequest {
    private String brand;
    private String partType;
    private String position;
    private String partNumber;

public void SkuResponse(String brand, String partType, String position, String partNumber){
    this.brand = brand;
    this.partType = partType;
    this.position = position;
    this.partNumber = partNumber;
}
    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
