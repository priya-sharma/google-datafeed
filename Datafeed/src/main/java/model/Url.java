package model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created by spineor on 27/9/17.
 */
@XmlRootElement
@XmlType(propOrder = { "loc", "lastmod"})
public class Url implements Serializable{
    private String loc;
    private String lastmod;


    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getLastmod() {
        return lastmod;
    }

    public void setLastmod(String lastmod) {
        this.lastmod = lastmod;
    }
}
