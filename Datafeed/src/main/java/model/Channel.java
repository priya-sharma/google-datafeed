package model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by spineor on 27/9/17.
 */
@XmlRootElement(name="channel")
@XmlSeeAlso(Item.class)
public class Channel  implements Serializable{


    private ArrayList<Item> itemList;

    @XmlElement(name = "item")
    public ArrayList<Item> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<Item> itemList) {
        this.itemList = itemList;
    }
}
