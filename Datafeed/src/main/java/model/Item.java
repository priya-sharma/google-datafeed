package model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created by spineor on 27/9/17.
 */
@XmlRootElement
@XmlType(propOrder = { "id", "title", "description", "link","image_link","availability","price"
        ,"product_type","brand","gtin","mpn","condition","item_group_id","custom_label_0","custom_label_1","google_product_category" })

public class Item  implements Serializable{
    private String id;
    private String title;
    private String description;
    private String link;
    private String image_link;
    private String availability;
    private String price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String product_type;
    private String brand;
    private String gtin;
    private String mpn;
    private String condition;
    private String item_group_id;
    private String custom_label_0;
    private String custom_label_1;
    private String google_product_category;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getItem_group_id() {
        return item_group_id;
    }

    public void setItem_group_id(String item_group_id) {
        this.item_group_id = item_group_id;
    }

    public String getTitle() {
        return title;

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCustom_label_0() {
        return custom_label_0;
    }

    public void setCustom_label_0(String custom_label_0) {
        this.custom_label_0 = custom_label_0;
    }

    public String getCustom_label_1() {
        return custom_label_1;
    }

    public void setCustom_label_1(String custom_label_1) {
        this.custom_label_1 = custom_label_1;
    }

    public String getGoogle_product_category() {
        return google_product_category;
    }

    public void setGoogle_product_category(String google_product_category) {
        this.google_product_category = google_product_category;
    }
}
