package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by spineor on 27/9/17.
 */
@XmlRootElement(name="urlset")
@XmlSeeAlso(Url.class)
public class Urlset implements Serializable{


    private ArrayList<Url> urlList;

    @XmlElement(name = "url")

    public ArrayList<Url> getUrlList() {
        return urlList;
    }

    public void setUrlList(ArrayList<Url> urlList) {
        this.urlList = urlList;
    }
}
