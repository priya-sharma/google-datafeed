package model;

/**
 * Created by spineor on 6/10/17.
 */
public class SkuResponse {

    private String partNumber;
    private String title;
    private String availability;
    private String description;
    private String imageList;
    private String manufacturer_number;
    private String category;
    private String subcategory;
    private String dimensionUOM;
    private String weightUOM;
    private String length;
    private String height;
    private String width;
    private String weight;
    private String itemLevelGTIN;
    private String price;
    private String brand;
    private String core_deposit;
    private String brand_type;
    private String condition;
    private String fitmentuid;
    private String product_url;
    private String oePartNumber;
    private String part_type;
    private String part;
    private String position;
    private String imageURL;
    private String certification;

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageList() {
        return imageList;
    }

    public void setImageList(String imageList) {
        this.imageList = imageList;
    }

    public String getManufacturer_number() {
        return manufacturer_number;
    }

    public void setManufacturer_number(String manufacturer_number) {
        this.manufacturer_number = manufacturer_number;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getDimensionUOM() {
        return dimensionUOM;
    }

    public void setDimensionUOM(String dimensionUOM) {
        this.dimensionUOM = dimensionUOM;
    }

    public String getWeightUOM() {
        return weightUOM;
    }

    public void setWeightUOM(String weightUOM) {
        this.weightUOM = weightUOM;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getItemLevelGTIN() {
        return itemLevelGTIN;
    }

    public void setItemLevelGTIN(String itemLevelGTIN) {
        this.itemLevelGTIN = itemLevelGTIN;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCore_deposit() {
        return core_deposit;
    }

    public void setCore_deposit(String core_deposit) {
        this.core_deposit = core_deposit;
    }

    public String getBrand_type() {
        return brand_type;
    }

    public void setBrand_type(String brand_type) {
        this.brand_type = brand_type;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getFitmentuid() {
        return fitmentuid;
    }

    public void setFitmentuid(String fitmentuid) {
        this.fitmentuid = fitmentuid;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }

    public String getOePartNumber() {
        return oePartNumber;
    }

    public void setOePartNumber(String oePartNumber) {
        this.oePartNumber = oePartNumber;
    }

    public String getPart_type() {
        return part_type;
    }

    public void setPart_type(String part_type) {
        this.part_type = part_type;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }
}
