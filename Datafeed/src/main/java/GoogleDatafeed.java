/**
 * Created by spineor on 16/11/17.
 */

import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.Datafeed;
import com.google.api.services.content.model.DatafeedFetchSchedule;
import com.google.api.services.content.model.DatafeedFormat;
import com.google.api.services.content.model.DatafeedsCustomBatchRequest;
import com.google.api.services.content.model.DatafeedsCustomBatchRequestEntry;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import config.ContentConfig;

import javax.xml.bind.JAXBException;

public class GoogleDatafeed {
    public static void main(String[] args)throws JAXBException, IOException, GeneralSecurityException {
        Authorization authorization = new Authorization();
        ShoppingContent service = authorization.getShoppingContent();
        insertfeed(service);
    }

    public static void insertfeed(ShoppingContent service)throws JAXBException, IOException{
        String url = "http://api.theautopartsshop.com/gfeed150k11decnew/";
        String file = "part-11-12-2017";
        Datafeed datafeed = new Datafeed();
        //datafeed.setId(12345L);
        datafeed.setName("YearRange");
        datafeed.setContentType("products");
        datafeed.setAttributeLanguage("en");
        datafeed.setContentLanguage("en");
        datafeed.setFileName(file);
        DatafeedFetchSchedule schedule = new DatafeedFetchSchedule();
        schedule.setWeekday("tuesday");
        schedule.setHour(5L);
        //schedule.setMinuteOfHour(50L);
        schedule.setTimeZone("America/Los_Angeles");
        schedule.setFetchUrl(url+"/"+file);

        datafeed.setFetchSchedule(schedule);
        datafeed.setTargetCountry("US");

        Datafeed result = service.datafeeds().insert(new BigInteger("115161870"), datafeed).execute();
        System.out.printf("Datafeed %s inserted with ID %d\n", result.getName(), result.getId());
    }
}
