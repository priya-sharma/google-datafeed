import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.Price;
import com.google.api.services.content.model.Product;
import com.google.api.services.content.model.ProductShipping;
import model.Channel;
import model.Item;
import model.OurPrice;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.naming.spi.ObjectFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

/**
 * Created by spineor on 27/9/17.
 */
public class XmlToObject {

    private static final BigInteger merchantId = new BigInteger("115161870");

    public static void main(String[] args)throws JAXBException, IOException, GeneralSecurityException {

        Authorization authorization = new Authorization();
        ShoppingContent service = authorization.getShoppingContent();
        insertProducts(service);
        //authorization.productWrapper(sc,);
        //.listProduct(sc);

    }
//"https://images.theautopartsshop.com/wp/hires/W01331610508OES.JPG"
    public static void insertProducts(ShoppingContent service)throws JAXBException, IOException{
        JAXBContext context = JAXBContext.newInstance(Channel.class);

        Unmarshaller unmarshaller = context.createUnmarshaller();
        for(int i=0;i<100 ; i++) {
            Channel e = (Channel) unmarshaller.unmarshal(new File("/home/spineor/taps/part-000"+((i<10)?"0"+i : i)));
            for (Item item : e.getItemList()) {
                if (item.getImage_link() != null) {

                    Product product = new Product();

                    product.setOfferId(item.getId());
                    product.setTitle(item.getTitle());
                    product.setDescription(item.getDescription());
                    product.setLink(item.getLink());
                    product.setImageLink(item.getImage_link());
                    //product.setImageLink("https://images.theautopartsshop.com/wp/hires/W01331610508OES.JPG");
                    product.setContentLanguage("en");
                    product.setTargetCountry("US");
                    product.setChannel("online");
                    product.setProductType(item.getProduct_type());
                    product.setAvailability(item.getAvailability());
                    product.setCondition(item.getCondition());
                    //product.setGoogleProductCategory(item.getProduct_type());
                    product.setGtin(item.getGtin());
                    product.setGoogleProductCategory(item.getGoogle_product_category());
                    product.setAdwordsRedirect("https://tracking.theautopartsshop.com/?campaignid={campaignid}&adgroupid={adgroupid}&feeditemid={feeditemid}&targetid={targetid}&matchtype={matchtype}&network={network}&device={device}&devicemodel={devicemodel}&creative={creative}&keyword={keyword}&placement={placement}&target={target}&adposition={adposition}&url=" + item.getLink());
                    product.setCustomLabel0(item.getCustom_label_0());
                    product.setCustomLabel1(item.getCustom_label_1());

                    String value = priceSplit(item.getPrice()).getPriceValue();
                    String currency = priceSplit(item.getPrice()).getPriceCurrency();

                    Price price = new Price();
                    price.setValue(value);
                    price.setCurrency("USD");
                    product.setPrice(price);

                    value = priceSplit(item.getPrice()).getPriceValue();
                    currency = priceSplit(item.getPrice()).getPriceCurrency();

                    Price shippingPrice = new Price();
                    shippingPrice.setValue("0.0");
                    shippingPrice.setCurrency("USD");

                    ProductShipping shipping = new ProductShipping();
                    shipping.setPrice(shippingPrice);
                    shipping.setCountry("US");
                    shipping.setService("Standard shipping");

                    ArrayList shippingList = new ArrayList();
                    shippingList.add(shipping);
                    product.setShipping(shippingList);

                    Product result = service.products().insert(merchantId, product).execute();
                    System.out.println("Insert Response: " + result);
                }
            }
        }
    }



    public static OurPrice priceSplit(String price){
        OurPrice oprice = new OurPrice();
        oprice.setPriceValue(price.split(" ")[0]);
        oprice.setPriceCurrency(price.split(" ")[1]);
        return oprice;
    }
}


