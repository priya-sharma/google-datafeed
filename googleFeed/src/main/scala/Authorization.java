import com.google.api.client.googleapis.auth.oauth2.*;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.Product;
import com.google.api.services.content.model.ProductsListResponse;
import com.google.common.collect.Lists;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.GeneralSecurityException;

public class Authorization {

    private static final String USER_AGENT = "Mozilla/5.0";

    private static final BigInteger merchantId = new BigInteger("115161870");

    private static final String datafeedId = "*";

   // private static final String GET_URL = "https://www.googleapis.com/content/v2/" + merchantId + "/datafeedstatuses/" + datafeedId;

   // private static final String POST_URL = "";

    //private static final String POST_PARAMS = "";

    public static void main(String[] args) throws IOException, GeneralSecurityException {


        getShoppingContent();


       // new Authorization().productWrapper(getShoppingContent(),product);
        //new Authorization().listProduct(getShoppingContent());
    }



    public void listProduct(ShoppingContent service) throws  IOException{
        ShoppingContent.Products.List productsList = service.products().list(merchantId);

        ProductsListResponse page = productsList.execute();
        while ((page.getResources() != null) && !page.getResources().isEmpty()) {
            for (Product product : page.getResources()) {

                System.out.printf("%s %s%n", product.getId(), product.getTitle());
            }

            if (page.getNextPageToken() == null) {
                break;
            }

            productsList.setPageToken(page.getNextPageToken());
            page = productsList.execute();
        }
    }

    public void productWrapper(ShoppingContent service,Product product) throws IOException{

        Product result = service.products().insert(Authorization.merchantId, product).execute();

    }

    public static ShoppingContent getShoppingContent()  throws IOException, GeneralSecurityException{
        String clientId = "828808872543-tju0hlus7njcu35g7i0masf2c9oo6v52.apps.googleusercontent.com";
        String clientSecretKey = "nKK99lJCOo4DsaeD0AGynpOF";
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        GoogleClientSecrets.Details details = new GoogleClientSecrets.Details();
        details.setClientId(clientId);
        details.setClientSecret(clientSecretKey);

        String redirectUrl = "urn:ietf:wg:oauth:2.0:oob";
        String scope = "https://www.googleapis.com/auth/content";
        GoogleClientSecrets clientSecrets = new GoogleClientSecrets();
        clientSecrets.setInstalled(details);
        GoogleAuthorizationCodeFlow authorizationFlow = new GoogleAuthorizationCodeFlow
                .Builder(httpTransport, jsonFactory, clientSecrets, Lists.newArrayList(scope))
                .setAccessType("offline").build();
        //String authorizeUrl =
          //      authorizationFlow.newAuthorizationUrl().setRedirectUri(redirectUrl).build();
        //System.out.println("Paste this url in your browser: \n" + authorizeUrl + '\n');

        // Wait for the authorization code.
        //System.out.println("Type the code you received here: ");
        //String authorizationCode = new BufferedReader(new InputStreamReader(System.in)).readLine();

        // Authorize the OAuth2 token.
        //GoogleAuthorizationCodeTokenRequest tokenRequest =
          //      authorizationFlow.newTokenRequest(authorizationCode);
        //tokenRequest.setRedirectUri(redirectUrl);
        //GoogleTokenResponse tokenResponse = tokenRequest.execute();

        // Create the OAuth2 credential.
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(new NetHttpTransport())
                .setJsonFactory(new JacksonFactory())
                .setClientSecrets(clientSecrets)
                .build();

        // Set authorized credentials.
        credential.setRefreshToken("1/jBCfi_0JgoEcT6hPAAB2iOGU9rY4Lgh4QyV3xGz-9-fvD3qP3lHcVC84vhX4bgdM");
        credential.refreshToken();
        System.out.println(credential.getRefreshToken());
        ShoppingContent service = new ShoppingContent
                .Builder(httpTransport, jsonFactory, credential)
                .setApplicationName("DataFeed")
                .build();
        return service;
    }

}