import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.*;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by spineor on 21/11/17.
 */
public class GetUpdateBatchInsertProduct {

    private static final BigInteger merchantId = new BigInteger("115161870");
    static Map map = new HashMap<String, String>();

    public static void main(String[] args) throws JAXBException, IOException, GeneralSecurityException {
        Authorization authorization = new Authorization();
        ShoppingContent service = authorization.getShoppingContent();
        getProductList(merchantId, service);
    }

    public static void getProductList(BigInteger merchantId, ShoppingContent content) throws IOException {
        ShoppingContent.Products.List productsList = content.products().list(merchantId).setMaxResults(250L);
        int batch = 0;
        int i = 0, j = 0;
        do {
            ProductsListResponse page = productsList.execute();
            if (page.getResources() == null) {
                System.out.println("No products found.");
                return;
            }
            i++;
            List<Product> products = new ArrayList<Product>();
            for (Product product : page.getResources()) {

                products.add(updateProduct(product));
                j++;
            }
            //System.out.println("ProductUpdate: " +products);

/*
            ProductsCustomBatchResponse batchResponse =
                    content.products().custombatch(createBatch(products)).execute();
*/

            batch++;
            System.out.println("processing batch " + batch);
            if (page.getNextPageToken() == null) {
                System.out.println("finished");
                break;
            }
            productsList.setPageToken(page.getNextPageToken());

        } while (true);
    }

    public static Product updateProduct(Product product) throws IOException {
        Product newProduct = new Product();
//        String staticAdWord = "?utm_source=GoogleShopping&utm_medium=cpc&utm_campaign={campaignid}";

        newProduct.setOfferId(product.getOfferId());
        newProduct.setId(product.getId());
        newProduct.setTitle(product.getTitle());

      ///////////  newProduct.setLink(product.getLink());

        if(product.getLink().contains("%2526")){
            newProduct.setLink(product.getLink().replace("%2526","%26"));
//            System.out.println(newProduct.getOfferId() + " : " + newProduct.getLink());
        }
        else {
            newProduct.setLink(product.getLink());
        }

        newProduct.setImageLink(product.getImageLink());
        newProduct.setContentLanguage(product.getContentLanguage());
        newProduct.setTargetCountry(product.getTargetCountry());
        newProduct.setChannel(product.getChannel());
        newProduct.setAvailability(product.getAvailability());
        newProduct.setBrand(product.getBrand());
        newProduct.setCondition(product.getCondition());
        newProduct.setGoogleProductCategory(product.getGoogleProductCategory());
        newProduct.setGtin(product.getGtin());
        newProduct.setItemGroupId(product.getItemGroupId());
        newProduct.setMpn(product.getMpn());
        newProduct.setPrice(product.getPrice());
        newProduct.setProductType(product.getProductType());
        newProduct.setShipping(product.getShipping());
        //newProduct.setAdwordsRedirect(product.getLink() + staticAdWord);

        if(product.getAdwordsRedirect().contains("%2526")){
            newProduct.setAdwordsRedirect(product.getAdwordsRedirect().replace("%2526", "%26"));
//            System.out.println(newProduct.getOfferId() + " : "  + newProduct.getAdwordsRedirect());
        }
        else {
            newProduct.setAdwordsRedirect(product.getAdwordsRedirect());
        }

        newProduct.setCustomLabel0(product.getCustomLabel0());
        newProduct.setCustomLabel1(product.getCustomLabel1());
        newProduct.setCustomLabel2(product.getCustomLabel2());
        newProduct.setDescription(product.getDescription());

        System.out.println(newProduct.getOfferId() + " : " + newProduct.getLink() + " : " + newProduct.getAdwordsRedirect());
        return newProduct;

    }

    public static ProductsCustomBatchRequest createBatch(List<Product> products) {
        List<ProductsCustomBatchRequestEntry> productsBatchRequestEntries =
                new ArrayList<ProductsCustomBatchRequestEntry>();
        for (int i = 0; i < products.size(); i++) {
            productsBatchRequestEntries.add(
                    new ProductsCustomBatchRequestEntry()
                            .setBatchId((long) i)
                            .setMerchantId(merchantId)
                            .setProduct(products.get(i))
                            .setMethod("insert"));
        }
        ProductsCustomBatchRequest batchRequest = new ProductsCustomBatchRequest();
        batchRequest.setEntries(productsBatchRequestEntries);
        return batchRequest;
    }
}

