name := "basic-project"

organization := "example"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.2"

crossScalaVersions := Seq("2.10.4", "2.11.2")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "org.scalacheck" %% "scalacheck" % "1.11.5" % "test"
)

libraryDependencies ++= Seq(
  "com.microsoft.bingads" % "microsoft.bingads" % "11.5.6",
  "com.google.apis" % "google-api-services-content" % "v2-rev77-1.21.0",
  "com.google.oauth-client" % "google-oauth-client-jetty" % "1.19.0",
  "com.google.apis" % "google-api-services-manufacturers" % "v1-rev19-1.22.0",
  "commons-cli" % "commons-cli" % "1.4"
)


initialCommands := "import example._"
